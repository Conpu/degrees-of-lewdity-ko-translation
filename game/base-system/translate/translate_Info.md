대부분의 결과물은 _trResult에 저장함
조사를 분리하여 저장하는 경우 _trPost에 저장함

'''
#폴더
*(.twee)파일
'''



-----------------
# Clothes

* trClothes
    <<trClothes>>
        <<trClothes *part *clothes_name>>
        <<trClothes *part *clothes_name *name post "sep">>
        <<trClothes *part *clothes_name *[desc | description]>>

        옷의 이름이나 설명을 번역한다.


        필수사항
        - *part: 착용 부위
        - *clothes_name: 옷의 이름
        - *name: 옷의 이름을 출력한다.
        - *desc , description: 옷의 설명을 출력한다.

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trClothes *upper "sundress">>                                 // _trResult: "여름용 원피스"
        <<trClothes "upper" "sundress" "name" "을" "sep">>              // _trResult: "여름용 원피스", _trPost: "를"
        <<trClothes "upper" "sundress" "desc">>                         // _trResult: "뛰어 놀기에 좋다."


    <<trSearchClothes>>
        <<trSearchClothes *clothes_name>>
        <<trSearchClothes *clothes_name *name post "sep">>
        <<trSearchClothes *clothes_name *[desc | description]>>

        
        옷 타입의 지정 없이 번역한다.
        +모든 항목과 대조한다.

        필수사항
        - *clothes_name: 옷의 이름
        - *name: 옷의 이름을 출력한다.
        - *desc , description: 옷의 설명을 출력한다.

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trSearchClothes "sundress">>                                 // _trResult: "여름용 원피스"
        <<trSearchClothes "sundress" "name" "을" "sep">>              // _trResult: "여름용 원피스", _trPost: "를"
        <<trSearchClothes "sundress" "desc">>                         // _trResult: "뛰어 놀기에 좋다."


* trClothesType
    <<trClothesType>>
        <<trClothesType *clothesType post>>

        옷의 타입을 번역합니다.

        필수사항
        - *clothesType: 옷의 타입
        
        선택사항
        - post: 번역결과 뒤에 해당 조사를 붙인다.

        e.g.
        <<trClothesType "upper">>               // _trResult: "상의"



-----------------
# Post 
    + 바로 출력하기 때문에 _trResult를 사용하지 않음
    + 이 폴더에 있는 모든 매크로는 표기가 다르더라도 'post' 필수사항임

* bodyPost
    <<beasttypePost>>
        <<beasttypePost post "sep">>

            <<beasttype>>의 대체 위젯. 조사를 붙일 수 있다.
        

            선택사항
            - post: 번역결과의 뒤에 조사를 붙인다.
            - sep: 조사를 분리하여 저장한다.

            e.g.
            <<beasttypePost "을">>                //<<breasts>>을 | <<breasts>>를


    <<breastsPost>>
        <<breastsPost post "sep">>

        <<breasts>>의 대체 위젯. 조사를 붙일 수 있다.
       

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<breastsPost "을">>                //<<breasts>>을 | <<breasts>>를


    <<bottomPost>>
        +<<breastsPost>>와 용도 및 방식 같음. 생략

    
    <<pussyPost>>
        +<<breastsPost>>와 용도 및 방식 같음. 생략

    
    <<genitalsPost>>
        <<genitalsPost *post num>>
        <<genitalsPost *post "sep" num>>

        <<genitals>>의 대체 위젯. 조사를 붙일 수 있다.

        필수사항
        - post: 조사

        선택사항
        - num: <<genitals>>에 원래 사용하는 인수값
        - sep: 조사를 분리하여 저장한다.

    <<penisPost>>
        +<<breastsPost>>와 용도 및 방식 같음. 생략


* clothesPost
    <<undertopPost>>
        <<undertopPost post "sep">>

        <<undertop>>의 대체 위젯. 조사를 붙일 수 있다.


        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<undertopPost "을">>               //<<trClothes "under_upper" $worn.under_upper.name "name" $args[0] $args[1]>> | <<breastsPost $args[0] $args[1]>>


    <<groinPost>>
        <<groinPost post>>


        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다. (sep은 사용할 수 없음에 유의.)


    <<crotchPost>>
        +<<undertopPost>>와 용도 및 방식 같음. 생략


    <<undiesPost>>
        +<<undertopPost>>와 용도 및 방식 같음. 생략


    <<bottomsPost>>
        +<<undertopPost>>와 용도 및 방식 같음. 생략


    <<underbottomsPost>>
        +<<undertopPost>>와 용도 및 방식 같음. 생략


    <<topPost>>
        +<<undertopPost>>와 용도 및 방식 같음. 생략

    
    <<topasidePost>>
        +<<undertopPost>>와 용도 및 방식 같음. 생략


    <<breastsasidePost>>
        +<<undertopPost>>와 용도 및 방식 같음. 생략



    <<outfitPost>>
        +<<undertopPost>>와 용도 및 방식 같음. 생략


* girlPost
    <<girlPost>>
        <<girlPost post "sep">>

        <<girl>>의 대체 위젯. 조사를 붙일 수 있다.


        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

    <<girlsPost>>
        +<<girlPost>>와 용도 및 방식 같음. 생략

    <<girlfriendPost>>
        +<<girlPost>>와 용도 및 방식 같음. 생략

    <<wifePost>>
        +<<girlPost>>와 용도 및 방식 같음. 생략

    <<victimgirlPost>>
        +<<girlPost>>와 용도 및 방식 같음. 생략

    <<victimgirlsPost>>
        +<<girlPost>>와 용도 및 방식 같음. 생략

    <<lassPost>>
        +<<girlPost>>와 용도 및 방식 같음. 생략

    <<genderPost>>
        +<<girlPost>>와 용도 및 방식 같음. 생략

    <<bitchPost>>
        +<<girlPost>>와 용도 및 방식 같음. 생략


* HePost
    <<HePost>>
        <<trHe pose "sep">>
        
        <<He>>의 대체 위젯. 조사를 붙일 수 있다.
        +<<hePost>> , <<HimPost>>, <<ShePost>>등도 사용할 수 있음.

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trHe "을">>               // <<He>>을 | <<He>>를


    <<pHePost>>
        <<trpHe pose "sep">>
        
        <<pHe>>의 대체 위젯. 조사를 붙일 수 있다.
        +<<phePost>> , <<pHimPost>>, <<pShePost>>등도 사용할 수 있음.

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trpHe "을">>               // <<pHe>>을 | <<pHe>>를


* personPost
    <<personPost>>
        <<Person post "sep">>

        <<person>>의 대체 위젯. 조사를 붙일 수 있다.

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.
        
        e.g.
        <<trPerson "을">>               // <<person>>을 | <<person>>를



* putpost
    <<putpost *target *post 'sep'>>

    단어 뒤에 조사를 붙인다.
    + 해당 파일의 _putpostList에 미리 정의되어 있어야 함.

    필수사항
    - target: 조사를 붙일 대상
    - post: 번역 결과의 뒤에 조사를 붙인다.

    선택사항
    - sep: 조사를 분리하여 저장한다.

    e.g.
    <<putpost "man" "이">>              // "남자가"



-----------------
# Tentacle

* trTentacle
    <<trTentacle>>
        <<trTentacle *tentacledesc post "sep">>

        촉수의 상세를 번역한다.
        #바로 출력하기 때문에 _trResult를 사용하지 않음

        필수사항
        - tentacledesc: 촉수의 상세

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        $tentacles[_i].fullDesc = "narrow tantacle"
        <<trTentacle $tentacles[_i].fullDesc "을">>              //"가느다란 촉수를"



-----------------
# Other

* trBodypart
    <<trBodypart>>
        <<trBodypart *bodyPart post "sep">>

        몸의 부위를 번역한다.

        필수사항
        - *bodyPart: 몸의 부위

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trBodypart 'left_arm' '을'>>_trResult             // '왼팔을'


* trBodyWriting
    <<trBodyWriting>>
        <<trBodyWriting *bodywriting post "sep">>

        바디라이팅을 번역한다.

        필수사항
        - *bodywriting

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trBodyWriting "heart">>               // _trResult: "하트"


* trBreastsdesc
    <<trBreastsdesc>>
        <<trBreastsdesc *Breastdesc post "sep">>

        가슴의 상세를 번역한다.

        필수사항
        - *Breastdesc: 가슴의 상세

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trBreastsdesc "budding breasts" "을">>             //"약간 솟아오른 가슴을"


* trClolur
    <<trColour>>
        <<trColour *colour>>

        색을 번역한다.

        필수사항
        - *colour: 번역할 색

        e.g.
        <<trColour 'red'>>


* trNamedNPC
    <<trNamedNPC>>
        <<trNamedNPC *npcName "title" post "sep">>
        <<trNamedNPC *npcName post "sep">>

        namedNPC를 번역한다.
        
        필수사항
        - *npcName: 번역할 NamedNPC의 이름

        선택사항
        - title: 해당 NPC의 이름 대신 타이틀을 출력한다.
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trNamedNPC River>>_trResult                // '리버'      
        <<trNamedNPC River "title">>_trResult          // '수학 교사' 
        <<trNamedNPC River '을'>>_trResult           // '리버를'    


* trNPCdesc
    <<trNPCdesc>>
        <<trNPCdesc *NPCdesc>>
        <<trNPCdesc *NPCfulldesc post "sep">>

        NPC의 상세(Adj)를 번역한다.
        +인수가 _NPCdesc에 없는 경우 인수 그대로 출력한다.

        필수사항
        - *NPCdesc: NPC의 상세
        - *NPCdesc: NPC의 모든 상세

        선택사항
        - title: 해당 NPC의 이름 대신 타이틀을 출력한다.
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trNPCdesc "shapely">>                         // _trResult: "균형잡힌"
        <<trNPCdesc "shapely girl" "을">>               // _trResult: "균형잡힌 소녀를"
        <<trNPCdesc "Robin" "을">>                      // _trResult: "로빈을"


* trParasite
    <<trParasite>>
        <<trParasite *parasite post "sep">>

        기생충을 번역한다.

        필수사항
        - *parasite: 기생충 이름

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trParasite tantacle '을'>>_trResult               // '촉수를'


* trPenisdesc
    <<trPenisdesc>>
        <<trPenisdesc *Penisdesc post "sep">>

        남성기의 상세를 번역한다.

        필수사항
        - *Penisdesc: 남성기의 상세

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trBreastsdesc "tiny penis" "을">>             //"자그마한 자지를"


* trPill
    <<trPill>>
        <<trPill *PillType>>

        약의 타입을 번역합니다.

        필수사항
        - *PillType: 약의 타입

        e.g.
        <<trPill "Growth">>     // 성장


* trPlants
    <<trPlants>>
        <<trPlants *'name' *plantName post "sep">>
        <<trPlants *'plural' *plantPlural post "sep">>

        식물을 번역한다.

        필수사항
        - *'name': 식물의 이름을 이용해서 번역
        - *plantName: 식물의 이름
        - *'plural': 식물의 복수형태 단어를 이용해서 번역
        - *plantPlural: 식물의 복수형 단어

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trPlants 'name' 'daisy' '을'>>_trResult               // '데이지를' 
        <<trPlants 'plural' 'daisies'>>_trResult                // '데이지'  


    <<trSearchPlants>>
        <<trSearchPlants *plant post "sep">>

        식물을 번역한다.

        필수사항
        - *plant: 식물

        선택사항
        - post: 번역결과의 뒤에 조사를 붙인다.
        - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trSearchPlants 'daisy' '을'>>_trResult               // '데이지를' 


* trPost
    <<trPost>>
        <<trPost *postNum *post "sep">>
        
        조사를 번역하여 _trResult의 뒤에 붙인다.

        필수사항
        - *postNum: 1이면 입력받은 조사의 형태를 변경한다.
        - *post: 조사

        선택사항
        - sep: 조사를 _trPost에 따로 저장한다.

        e.g.
        /* _trResult = "가나다" */
        <<trPost 0 '은'>>          // _trResult: '가다나은'
        /* _trResult = "abc" */
        <<trPost 1 '을'>>          // _trResult: 'abc를'
        /* _trResult = "123" */
        <<trPost 1 '이'>>          // _trResult: '123', _trPost: "가"


* trWeather
    <<trWeather>>
        <<trWeather *weather post "sep">>

        날씨를 번역합니다.

        필수사항
        - *weather: 날씨

        선택사항
            - post: 번역결과의 뒤에 조사를 붙인다.
            - sep: 조사를 분리하여 저장한다.

        e.g.
        <<trWeather "rain">>                // _trResult: "비"

